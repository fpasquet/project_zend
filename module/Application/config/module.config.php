<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'doctrine' => array(
        'driver' => array(
            'odm_driver' => array(
                'class' => 'Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/Application/Document')
            ),
            'odm_default' => array(
                'drivers' => array(
                    'Application\Document' => 'odm_driver'
                )
            )
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\PersonRest' => 'Application\Controller\PersonRestController',
            'Application\Controller\CountryRest' => 'Application\Controller\CountryRestController',
            'Application\Controller\GeolocRest' => 'Application\Controller\GeolocRestController',
            'Application\Controller\SwaggerRest' => 'Application\Controller\SwaggerRestController'
        ),
    ),
    // The following section is new` and should be added to your file
    'router' => array(
        'routes' => array(
            'person' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/api/persons[/:id][:format]',
                    'constraints' => array(
                        'id'     => '[0-9|a-z]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\PersonRest',
                    ),
                ),
            ),
            'country' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/api/countries[/:id]',
                    'constraints' => array(
                        'id'     => '[0-9|a-z]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\CountryRest',
                    ),
                ),
            ),
            'geoloc' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/api/geoloc',
                    'defaults' => array(
                        'controller' => 'Application\Controller\GeolocRest',
                    ),
                )
            ),
            'swagger' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/api/swagger.json',
                    'defaults' => array(
                        'controller' => 'Application\Controller\SwaggerRest',
                    ),
                )
            )
        ),
    ),
    'service_manager' => [
        'invokables' => [
            'serializer' => 'Application\Service\Serializer',
            'geocode' => 'Application\Service\Geocode'
        ]
    ],
    'view_manager' => array( //Add this config
        'strategies' => array(
            'ViewJsonStrategy',
            'ViewXmlStrategy'
        )
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
                'doctrine-mongodb-fixtures-load' => array(
                    'options' => array(
                        'route'    => 'doctrine-mongodb-fixtures-load [--verbose|-v]',
                        'defaults' => array(
                            'controller' => 'Application\Controller\Index',
                            'action'     => 'fixturesLoad'
                        )
                    )
                )
            )
        )
    )
);