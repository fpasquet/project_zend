<?php

namespace Application\Controller;

use Application\Service\Serializer;
use Doctrine\ODM\MongoDB\DocumentManager;
use Zend\Mvc\Controller\AbstractRestfulController;

class AbstractRestController extends AbstractRestfulController
{
    /**
     * @return Serializer
     */
    public function getSerializer() {
        return $this->getServiceLocator()
            ->get('serializer');
    }

    /**
     * @return DocumentManager
     */
    public function getObjectManager() {
        return $this
            ->getServiceLocator()
            ->get('doctrine.documentmanager.odm_default');
    }

    /**
     * @return \Doctrine\ODM\MongoDB\DocumentRepository
     */
    public function getRepository($classNameDocument) {
        return $this->getObjectManager()
            ->getRepository($classNameDocument);
    }
}