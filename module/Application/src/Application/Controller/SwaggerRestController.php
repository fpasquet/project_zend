<?php

namespace Application\Controller;

use Application\Service\ReflectionClass;
use Zend\View\Model\JsonModel;

class SwaggerRestController extends AbstractRestController
{
    /**
     * @param array $data
     * @return JsonModel
     */
    public function getList()
    {
        $swagger = [
            "swagger" => "2.0",
            "info" => [
                "title" => "Api Zend Framework 2",
                "description" => "",
                "version" => "1.0.0"
            ],
            "host" => $_SERVER['HTTP_HOST'],
            "basePath" => "/api",
            "schemes" => [
                "http"
            ],
            "tags" => [
                [
                    "name" => "geolocalization"
                ]
            ],
            "paths" => []
        ];

        $resourceNameList = [
            'persons' => 'person',
            'countries' => 'country'
        ];
        foreach ($resourceNameList as $resourceNamePluralize => $resourceName) {
            $swagger = $this->addTag($swagger, $resourceNamePluralize);
            $swagger = $this->addCrud($swagger, $resourceName, $resourceNamePluralize);
        }

        $swagger["paths"]["/geoloc"] = [
            "post" => [
                "tags" => [
                    "geolocalization"
                ],
                "summary" => "Geolocalization",
                "description" => "",
                "consumes" => [
                    "application/x-www-form-urlencoded"
                ],
                "produces" => [
                    "application/json"
                ],
                "parameters" => [
                    [
                        "name" => "address",
                        "in" => "formData",
                        "required" => false,
                        "type" => "string"
                    ],
                    [
                        "name" => "city",
                        "in" => "formData",
                        "required" => false,
                        "type" => "string"
                    ],
                    [
                        "name" => "postalCode",
                        "in" => "formData",
                        "required" => false,
                        "type" => "string"
                    ],
                    [
                        "name" => "distance",
                        "in" => "formData",
                        "required" => false,
                        "type" => "number"
                    ]
                ]
            ]
        ];

        return new JsonModel($swagger);
    }

    public function addDefinitions($swagger)
    {
        $definitions = [];
        $documentClassList = scandir(__DIR__ . '/../Document', 1);
        foreach ($documentClassList as $documentClass) {
            $className = substr($documentClass, 0, -4);
            if ($className && !in_array($className, ['Document'])) {
                $resourceClassName = 'Application\Document\\' . $className;
                if (!in_array($className, ['Coordinates'])) {
                    $metadata = $this
                        ->getObjectManager()
                        ->getClassMetadata($resourceClassName);
                    $fieldMappings = $metadata->fieldMappings;
                }
                $reflect = new \ReflectionClass($resourceClassName);
                $definitions[$reflect->getShortName()] = [
                    "type" => "object",
                    "properties" => []
                ];
                foreach ($reflect->getProperties() as $property) {
                    $propertyName = $property->getName();
                    if (!in_array($propertyName, ['id', 'sm', 'coordinates', 'distance'])) {
                        $propertiesSwagger = [];
                        if (isset($fieldMappings)) {
                            switch ($fieldMappings[$propertyName]['type']) {
                                case "id":
                                case "integer":
                                    $propertiesSwagger["type"] = "integer";
                                    break;
                                case "float":
                                    $parameter["type"] = "number";
                                case "date":
                                    $propertiesSwagger["type"] = "string";
                                    $propertiesSwagger["format"] = "date-time";
                                    break;
                                default:
                                    $propertiesSwagger["type"] = "string";
                                    break;
                            }
                        } else {
                            $propertiesSwagger["type"] = "string";
                        }
                        $definitions[$reflect->getShortName()]['properties'][$propertyName] = $propertiesSwagger;
                    }
                }
            }
        }

        $swagger['definitions'] = $definitions;

        return $swagger;
    }

    public function getParameters($resourceClassName)
    {
        $parameters = [];

        $metadata = $this
            ->getObjectManager()
            ->getClassMetadata($resourceClassName);
        $fieldMappings = $metadata->fieldMappings;
        $reflect = new ReflectionClass($resourceClassName);

        foreach ($reflect->getPropertiesName() as $propertyName) {
            if (!in_array($propertyName, ['id', 'sm', 'coordinates', 'distance'])) {
                $parameter = [
                    "name" => $propertyName,
                    "in" => "formData",
                    "required" => false
                ];
                switch ($fieldMappings[$propertyName]['type']) {
                    case "id":
                    case "integer":
                        $parameter["type"] = "integer";
                        break;
                    case "float":
                        $parameter["type"] = "number";
                        break;
                    case "date":
                        $parameter["type"] = "string";
                        $parameter["format"] = "date-time";
                        break;
                    default:
                        $parameter["type"] = "string";
                        break;
                }
                array_push($parameters, $parameter);
            }
        }

        return $parameters;
    }

    public function addTag($swagger, $resourceNamePluralize)
    {
        array_push($swagger['tags'], [
            "name" => $resourceNamePluralize
        ]);

        return $swagger;
    }

    public function addCrud($swagger, $resourceName, $resourceNamePluralize)
    {
        $parameters = $this->getParameters('Application\Document\\' . ucfirst($resourceName));

        $parametersGet = [
            [
                "name" => "fields",
                "in" => "query",
                "description" => "The fields that will be displayed",
                "required" => false,
                "type" => "string"
            ],
            [
                "name" => "page",
                "in" => "query",
                "description" => "Current page",
                "required" => false,
                "type" => "integer"
            ],
            [
                "name" => "per_page",
                "in" => "query",
                "description" => "Number of items per page",
                "required" => false,
                "type" => "integer"
            ],
            [
                "name" => "sort",
                "in" => "query",
                "description" => "Sorting on attributes separated per semicolon",
                "required" => false,
                "type" => "string"
            ],
            [
                "name" => "desc",
                "in" => "query",
                "description" => "Default sorting is ascending, contains the names of the attributes that will be sorted in a descending manner",
                "required" => false,
                "type" => "string"
            ]
        ];
        $parametersGetSearch = $parametersGet;
        foreach ($parameters as $parameter) {
            $parameter['description'] = 'filter by field ' . $parameter["name"];
            $parameter['in'] = "query";
            array_push($parametersGet, $parameter);
        }

        $paths["/$resourceNamePluralize"] = [
            "get" => [
                "tags" => [
                    "$resourceNamePluralize"
                ],
                "summary" => "Finds $resourceNamePluralize",
                "description" => "",
                "consumes" => [
                    "application/json"
                ],
                "produces" => [
                    "application/json"
                ],
                "parameters" => $parametersGet,
                "responses" => [
                    "200" => [
                        "description" => "Finds $resourceNamePluralize"
                    ],
                    "400" => [
                        "description" => "This format is not accepted !"
                    ]
                ]
            ],
            "post" => [
                "tags" => [
                    "$resourceNamePluralize"
                ],
                "summary" => "Add a new $resourceName",
                "description" => "",
                "consumes" => [
                    "application/x-www-form-urlencoded"
                ],
                "produces" => [
                    "application/json"
                ],
                "parameters" => $parameters,
                "responses" => [
                    "204" => [
                        "description" => "Add a new $resourceName"
                    ],
                    "400" => [
                        "description" => "This format is not accepted !"
                    ]
                ]
            ]
        ];
        $paths["/$resourceNamePluralize/{id}"] = [
            "get" => [
                "tags" => [
                    "$resourceNamePluralize"
                ],
                "summary" => "Find $resourceName by id",
                "description" => "Returns a single " . $resourceName,
                "consumes" => [
                    "application/json"
                ],
                "produces" => [
                    "application/json"
                ],
                "parameters" => [
                    [
                        "name" => "id",
                        "in" => "path",
                        "description" => "id of $resourceName that needs to be fetched",
                        "required" => true,
                        "type" => "integer",
                        "maximum" => 5,
                        "minimum" => 1,
                        "format" => "int64"
                    ]
                ],
                "responses" => [
                    "200" => [
                        "description" => "Find $resourceName by id"
                    ],
                    "400" => [
                        "description" => "This format is not accepted !"
                    ],
                    "404" => [
                        "description" => "The $resourceName with this id doesn't exist"
                    ]
                ]
            ],
            "put" => [
                "tags" => [
                    "$resourceNamePluralize"
                ],
                "summary" => "Update an existing $resourceName",
                "description" => "",
                "consumes" => [
                    "application/json"
                ],
                "produces" => [
                    "application/json"
                ],
                "parameters" => array_merge([
                    [
                        "name" => "id",
                        "in" => "path",
                        "description" => "id $resourceName to update",
                        "required" => true,
                        "type" => "integer",
                        "maximum" => 5,
                        "minimum" => 1,
                        "format" => "int64"
                    ]
                ], $parameters),
                "responses" => [
                    "201" => [
                        "description" => "Update an existing $resourceName"
                    ],
                    "400" => [
                        "description" => "This format is not accepted !"
                    ],
                    "404" => [
                        "description" => "The $resourceName with this id doesn't exist"
                    ]
                ]
            ],
            "delete" => [
                "tags" => [
                    "$resourceNamePluralize"
                ],
                "summary" => "Delete a $resourceName",
                "description" => "",
                "consumes" => [
                    "application/json"
                ],
                "produces" => [
                    "application/json"
                ],
                "parameters" => [
                    [
                        "name" => "id",
                        "in" => "path",
                        "description" => "id $resourceName to delete",
                        "required" => true,
                        "type" => "integer",
                        "maximum" => 5,
                        "minimum" => 1,
                        "format" => "int64"
                    ]
                ],
                "responses" => [
                    "204" => [
                        "description" => "Delete a $resourceName"
                    ],
                    "400" => [
                        "description" => "This format is not accepted !"
                    ],
                    "404" => [
                        "description" => "The $resourceName with this id doesn't exist"
                    ]
                ]
            ]
        ];
//        $paths["/$resourceNamePluralize/search"] = [
//            "get" => [
//                "tags" => [
//                    "$resourceNamePluralize"
//                ],
//                "summary" => "Finds $resourceNamePluralize",
//                "description" => "",
//                "consumes" => [
//                    "application/json"
//                ],
//                "produces" => [
//                    "application/json"
//                ],
//                "parameters" => $parametersGetSearch,
//                "responses" => [
//                    "200" => [
//                        "description" => ""
//                    ]
//                ]
//            ]
//        ];
        foreach ($paths as $path => $methods) {
            $swagger['paths'][$path] = $methods;
        }
        return $swagger;
    }
}