<?php

namespace Application\Controller;

use Application\Service\ReflectionClass;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Doctrine\ODM\MongoDB\DocumentNotFoundException;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\Http\Response;
use DoctrineMongoODMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Zend\Paginator\Paginator;
use AP_XmlStrategy\View\Model\XmlModel;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class AbstractCrudRestController extends AbstractRestController
{
    protected $acceptFormat = ['json', 'xml'];

    /**
     * @var string
     */
    protected $classNameDocument;

    public function __construct($classNameDocument)
    {
        $this->classNameDocument = $classNameDocument;
    }

    public function renderView($data) {
        $format = substr($this->params('format'), 1);
        if (!empty($format)) {
            if (in_array($format, $this->acceptFormat)) {
                switch($format) {
                    case 'json':
                        return new JsonModel($data);
                        break;
                    case 'xml':
                        return new XmlModel($data);
                        break;
                }
            } else {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                return new JsonModel([
                    'error' => 'invalid_request',
                    'error_description' => 'This format is not accepted !'
                ]);
            }
        } else {
            return new JsonModel($data);
        }
    }


    /**
     * @return JsonModel
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getList()
    {
        /* Parameters */
        $fields = $this->params()->fromQuery('fields') ? explode(',', $this->params()->fromQuery('fields')) : null;
        $perPage = $this->params()->fromQuery('per_page') ? (int)$this->params()->fromQuery('per_page') : 10;
        $page = $this->params()->fromQuery('page') ? (int)$this->params()->fromQuery('page') : 1;
        $sortParams = $this->params()->fromQuery('sort') ? explode(',', $this->params()->fromQuery('sort')) : ['id'];
        $descParams = $this->params()->fromQuery('desc') ? explode(',', $this->params()->fromQuery('desc')) : [];

        $queryBuilder = $this->getRepository($this->classNameDocument)
            ->createQueryBuilder('d');
        if (!empty($fields)) {
            $select = [];
            foreach($fields as $field) {
                if (preg_match('/(\w+)\(([a-z,]+)\)/', $field, $match)) {
                    array_push($select, $match[1]);
                } else {
                    array_push($select, $field);
                }
            }
            $queryBuilder->select($select);
        }

        foreach ($sortParams as $sort) {
            $queryBuilder->sort($sort, in_array($sort, $descParams) ? 'desc' : 'asc');
        }

        $reflect = new ReflectionClass($this->classNameDocument);
        foreach ($reflect->getPropertiesName() as $propertyName) {
            $filter = $this->params()->fromQuery($propertyName) ? explode(',', $this->params()->fromQuery($propertyName)) : null;
            if (isset($filter)) {
                $queryBuilder->field($propertyName)->in($filter);
            }
        }

        $cursor = $queryBuilder->getQuery()->execute();

        $adapter = new DoctrineAdapter($cursor);
        $paginator = new Paginator($adapter);
        $paginator->setDefaultItemCountPerPage($perPage);

        $paginator->setCurrentPageNumber($page);

        /* Factory */
        $resources = [];
        foreach ($paginator->getCurrentItems() as $current) {
            if (isset($fields)) {
                $resource = $this->getSerializer()->serializeDocument($current, $fields);
            } else {
                $resource = $this->getSerializer()->serializeDocument($current);
            }
            array_push($resources, $resource);
        }

        return $this->renderView([
            'info' => [
                'nb_results' => $paginator->getCurrentItemCount(),
                'nb_pages' => $paginator->count(),
                'per_page' => $perPage,
                'page' => $page
            ],
            'resources' => $resources
        ]);
    }

    /**
     * @param integer $id
     * @return JsonModel
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function get($id)
    {
        $resource = $this->findByIdOr404($id);
        if ($resource instanceof ViewModel) {
            return $resource;
        }

        return $this->renderView($this->getSerializer()->serializeDocument($resource));
    }

    /**
     * @param array $data
     * @return JsonModel
     */
    public function create($data)
    {
        return $this->save($data);
    }

    /**
     * @param integer $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data)
    {
        return $this->save($data, $id);
    }

    /**
     * @param array $data
     * @param null|integer $id
     * @return JsonModel
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function save($data, $id = null)
    {
        $resource = isset($id) ? $this->findByIdOr404($id) : new $this->classNameDocument();
        if ($resource instanceof ViewModel) {
            return $resource;
        }
        $statusCode = isset($id) ? Response::STATUS_CODE_201 : Response::STATUS_CODE_204;

        $hydrator = new DoctrineObject(
            $this->getObjectManager(),
            $this->classNameDocument
        );

        $form = $this->getForm();
        $resource = $hydrator->hydrate($data, $resource);
        $form->bind($resource);
        if ($form->isValid()) {
            $this->getObjectManager()->persist($resource);
            $this->getObjectManager()->flush();
        } else {
            return $this->renderView(['errors' => $form->getMessages()]);
        }

        $response = $this->getResponse();
        $response->setStatusCode($statusCode);
        if (isset($id)) {
            $response->getHeaders()->addHeaders([
                'Location' => $this->url()->fromRoute('person', array('id' => $resource->getId()))
            ]);
        }

        return $this->renderView($resource);
    }

    /**
     * @param integer $id
     * @return JsonModel
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function delete($id)
    {
        $resource = $this->findByIdOr404($id);
        if ($resource instanceof ViewModel) {
            return $resource;
        }

        $this->getObjectManager()->remove($resource);
        $this->getObjectManager()->flush();

        $response = $this->getResponse()
            ->setStatusCode(Response::STATUS_CODE_204);

        return $response;

    }

    /**
     * @param integer $id
     *
     * @return object
     *
     * @throws DocumentNotFoundException
     */
    public function findByIdOr404($id)
    {
        $resource = $this->getRepository($this->classNameDocument)->find($id);
        $className = $this->classNameDocument;

        if (!$resource instanceof $className) {
            $splitClassName = explode('\\', $this->classNameDocument);
            $nameResource = end($splitClassName);

            $response = $this->getResponse();
            $response
                ->setStatusCode(Response::STATUS_CODE_404);

            return $this->renderView([
                "error" => "not_found",
                "error_description" => "The $nameResource with the id '$id' doesn't exist"
            ]);
        }

        return $resource;
    }

    /**
     * @return Form
     */
    public function getForm()
    {
        $builder = new AnnotationBuilder();
        $form = $builder->createForm($this->classNameDocument);

        return $form;
    }
}