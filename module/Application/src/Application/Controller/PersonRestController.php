<?php

namespace Application\Controller;

class PersonRestController extends AbstractCrudRestController
{
    public function __construct() {
        parent::__construct('Application\Document\Person');
    }
}