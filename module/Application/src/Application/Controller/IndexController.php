<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Console\Request as ConsoleRequest;
use Nelmio\Alice\Fixtures;

class IndexController extends AbstractActionController
{

    /**
     * @return DocumentManager
     */
    public function getObjectManager() {
        return $this
            ->getServiceLocator()
            ->get('doctrine.documentmanager.odm_default');
    }

    public function fixturesLoadAction()
    {
        $request = $this->getRequest();

        // Make sure that we are running in a console and the user has not tricked our
        // application into running this action from a public web server.
        if (!$request instanceof ConsoleRequest) {
            throw new \RuntimeException('You can only use this action from a console!');
        }

        $verbose = $request->getParam('verbose') || $request->getParam('v');

        Fixtures::load(__DIR__.'/../DataFixtures/MongoDB/fixtures.yml', $this->getObjectManager());

        if (!$verbose) {
            return "Done! Load data fixtures to your mongodb.";
        }else{
            return "Done! Load data fixtures to your mongodb.";
        }

    }
}