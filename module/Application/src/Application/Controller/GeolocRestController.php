<?php

namespace Application\Controller;

use Application\Service\Geocode;
use Doctrine\ODM\MongoDB\Query\Query;
use Zend\View\Model\JsonModel;

class GeolocRestController extends AbstractRestController
{
    /**
     * @param array $data
     * @return JsonModel
     */
    public function create($data)
    {
        if (!isset($data['distance'])) {
            throw new \Exception("The distance is not defined !");
        }

        $addressArray = [];
        if (isset($data['city'])) {
            array_push($addressArray, $data['city']);
        }
        if (isset($data['address'])) {
            array_push($addressArray, $data['address']);
        }
        if (isset($data['postalCode'])) {
            array_push($addressArray, $data['postalCode']);
        }

        if (empty($addressArray)) {
            throw new \Exception("The address is not defined !");
        }

        if (!isset($data['distance'])) {
            throw new \Exception("The distance is not defined !");
        }
        $distance = floatval($data['distance']);

        $geocode = new Geocode();
        $geocode->setAddress(implode('+', $addressArray));

        /**
         * @var Query
         */
        $qb = $this->getRepository('Application\Document\Person')
            ->createQueryBuilder('d')
            ->geoNear($geocode->getLatitude(), $geocode->getLongitude())
            ->spherical(true)
            ->distanceMultiplier(6378.13);

        $persons = $qb
            ->getQuery()
            ->execute();

        $data = [];
        foreach ($persons->toArray() as $person) {
            if (floatval($person->distance) <= $distance) {
            $personSerialized = $this->getSerializer()->serializeDocument($person);
            array_push($data, $personSerialized);
            }
        }

        return new JsonModel(['data' => $data]);
    }
}