<?php

namespace Application\Service;

class Serializer
{

    /**
     * @param $document
     * @param null|array $fields
     * @return array
     */
    public function serializeDocument($document, $fields = null)
    {
        $resource = [];
        $className = get_class($document);
        $reflect = new ReflectionClass($className);

        if (isset($fields)) {
            foreach ($fields as $field) {
                if (preg_match('/(\w+)\(([a-z,]+)\)/', $field, $match)) {
                    $propertyName = $match[1];
                    if (in_array($propertyName, $reflect->getPropertiesName())) {
                        $fieldsAdditional = is_array($match[2]) ? explode(',', $match[2]) : (array)$match[2];
                        $resource[$propertyName] = $this->setValueResource($resource, $reflect, $document, $propertyName, $fieldsAdditional);
                    }
                } elseif (in_array($field, $reflect->getPropertiesName())) {
                    $resource[$field] = $this->setValueResource($resource, $reflect, $document, $field);
                }
            }
        } else {
            foreach ($reflect->getPropertiesName() as $propertyName) {
                $resource[$propertyName] = $this->setValueResource($resource, $reflect, $document, $propertyName);
            }
        }
        return $resource;
    }

    /**
     * @param $resource
     * @param ReflectionClass $reflect
     * @param $document
     * @param $propertyName
     * @param null|array $fieldsAdditional
     */
    public function setValueResource($resource, $reflect, $document, $propertyName, $fieldsAdditional = null)
    {
        $methodName = 'get' . ucfirst($propertyName);
        $valueResource = null;
        if ($reflect->hasMethod($methodName)) {
            $value = $document->$methodName();
            if (!is_object($value)) {
                $valueResource = $value;
            } else {
                $valueResource = $this->serializeDocument($value, $fieldsAdditional);
            }
        }
        return $valueResource;
    }

}