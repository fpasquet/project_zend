<?php

namespace Application\Service;

class ReflectionClass extends \ReflectionClass
{
    public function getPropertiesName()
    {
        $propertiesName = [];
        foreach ($this->getProperties() as $property) {
            $propertyName = $property->getName();
            $methodName = 'get' . ucfirst($propertyName);
            if ($this->hasMethod($methodName)) {
                array_push($propertiesName, $propertyName);
            }
        }

        return $propertiesName;
    }
}