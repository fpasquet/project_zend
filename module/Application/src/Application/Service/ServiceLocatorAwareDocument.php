<?php

namespace Application\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ServiceLocatorAwareDocument implements ServiceLocatorAwareInterface
{
    /**
     * @var ServiceLocatorAwareInterface
     */
    protected $sm;

    /**
     * Set the service locator
     *
     * @param ServiceLocatorInterface $sm
     *
     * @return void
     */
    public function setServiceLocator(ServiceLocatorInterface $sm)
    {
        $this->sm = $sm;
    }

    /**
     * @return ServiceLocatorAwareInterface
     */
    public function getServiceLocator()
    {
        return $this->sm;
    }
}