<?php

namespace Application\Service;

use Zend\Http\Client;

class Geocode
{
    /**
     * @var string
     */
    protected $baseUrlApi;

    /**
     * @var string
     */
    protected $api;

    /**
     * @var null|string
     */
    protected $key = null;

    /**
     * @var string
     */
    protected $format;

    /**
     * @var string
     */
    protected $region;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var array
     */
    protected $response;

    public function __construct()
    {
        $this->baseUrlApi = 'https://maps.googleapis.com/maps/api/geocode';
        $this->format = 'json';
        $this->region = 'FR';
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     * @return Geocode
     */
    public function setFormat($format)
    {
        if (in_array(['json', 'xml'], $format)) {
            $this->format = $format;
        }
        return $this;
    }

    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     * @return Geocode
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return Geocode
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Geocode
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getApi()
    {
        $this->api = $this->baseUrlApi . '/' . $this->getFormat();
        $parameters = [];

        if ($this->getAddress()) {
            array_push($parameters, 'address=' . $this->getAddress());
        }
        if ($this->getRegion()) {
            array_push($parameters, 'region=' . $this->getRegion());
        }
        if ($this->getKey()) {
            array_push($parameters, 'key=' . $this->getKey());
        }

        $this->api .= '?' . implode('&', $parameters);

        return $this->api;
    }

    /**
     * @return array
     */
    public function getResponse()
    {
        if (!$this->response || $this->getApi() !== $this->api) {
            $client = new Client($this->getApi());
            $adapter = new Client\Adapter\Curl();
            $client->setAdapter($adapter);
            $response = $client->send();

            $data = json_decode($response->getBody(), true);

            $this->response = current($data['results']);
        }

        return $this->response;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        $response = $this->getResponse();

        return $response['geometry']['location']['lat'];

    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        $response = $this->getResponse();

        return $response['geometry']['location']['lng'];
    }
}