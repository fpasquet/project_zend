<?php

namespace Application\Document;


use Application\Service\ReflectionClass;
use Application\Service\ServiceLocatorAwareDocument;

abstract class Document extends ServiceLocatorAwareDocument
{
    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $reflect = new ReflectionClass(get_class($this));
        foreach ($reflect->getPropertiesName() as $propertyName) {
            $this->$propertyName = (!empty($data[$propertyName])) ? $data[$propertyName] : $this->$propertyName;
        }
    }
}