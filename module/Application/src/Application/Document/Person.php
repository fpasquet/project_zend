<?php

namespace Application\Document;

use Application\Service\Geocode;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Zend\Form\Annotation;

/**
 * @MongoDB\Document
 * @MongoDB\HasLifecycleCallbacks
 * @MongoDB\Index(keys={"coordinates"="2d"})
 */
class Person extends Document
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\String
     * @Annotation\Required({"required":"true"})
     */
    protected $email;

    /**
     * @MongoDB\Field(name="given_name", type="string")
     */
    protected $givenName;

    /**
     * @MongoDB\Field(name="family_name", type="string")
     */
    protected $familyName;

    /**
     * @MongoDB\Field(name="birth_date", type="date")
     */
    protected $birthDay;

    /**
     * @MongoDB\Field(name="birth_place", type="string")
     */
    protected $birthPlace;

    /**
     * @MongoDB\String
     */
    protected $gender;

    /**
     * @MongoDB\Field(name="job_title", type="string")
     */
    protected $jobTitle;

    /**
     * @MongoDB\String
     */
    protected $telephone;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Country")
     */
    protected $nationality;

    /**
     * @MongoDB\String
     */
    protected $address;

    /**
     * @MongoDB\Field(name="city", type="string")
     */
    protected $city;

    /**
     * @MongoDB\Field(name="postal_code", type="string")
     */
    protected $postalCode;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Country")
     */
    protected $country;

    /**
     * @MongoDB\EmbedOne(targetDocument="Coordinates")
     */
    public $coordinates;

    /**
     * @MongoDB\Distance
     */
    public $distance;

    /**
     * @MongoDB\PreFlush
     */
    public function updateGeographicCoordinates()
    {
        $addressArray = [];
        if (isset($this->address)) {
            array_push($addressArray, $this->address);
        }
        if (isset($this->city)) {
            array_push($addressArray, $this->city);
        }
        if (isset($this->postalCode)) {
            array_push($addressArray, $this->postalCode);
        }
        if (!empty($addressArray)) {
            $geocode = new Geocode();
            $geocode->setAddress(implode('+', $addressArray));
            $coordinates = new Coordinates();
            $coordinates->setX($geocode->getLatitude());
            $coordinates->setY($geocode->getLongitude());
            $this->setCoordinates($coordinates);
        }
    }



    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set givenName
     *
     * @param string $givenName
     * @return self
     */
    public function setGivenName($givenName)
    {
        $this->givenName = $givenName;
        return $this;
    }

    /**
     * Get givenName
     *
     * @return string $givenName
     */
    public function getGivenName()
    {
        return $this->givenName;
    }

    /**
     * Set familyName
     *
     * @param string $familyName
     * @return self
     */
    public function setFamilyName($familyName)
    {
        $this->familyName = $familyName;
        return $this;
    }

    /**
     * Get familyName
     *
     * @return string $familyName
     */
    public function getFamilyName()
    {
        return $this->familyName;
    }

    /**
     * Set birthDay
     *
     * @param date $birthDay
     * @return self
     */
    public function setBirthDay($birthDay)
    {
        $this->birthDay = $birthDay;
        return $this;
    }

    /**
     * Get birthDay
     *
     * @return date $birthDay
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * Set birthPlace
     *
     * @param string $birthPlace
     * @return self
     */
    public function setBirthPlace($birthPlace)
    {
        $this->birthPlace = $birthPlace;
        return $this;
    }

    /**
     * Get birthPlace
     *
     * @return string $birthPlace
     */
    public function getBirthPlace()
    {
        return $this->birthPlace;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * Get gender
     *
     * @return string $gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set jobTitle
     *
     * @param string $jobTitle
     * @return self
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;
        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string $jobTitle
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return self
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string $telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set nationality
     *
     * @param Application\Document\Country $nationality
     * @return self
     */
    public function setNationality(\Application\Document\Country $nationality)
    {
        $this->nationality = $nationality;
        return $this;
    }

    /**
     * Get nationality
     *
     * @return Application\Document\Country $nationality
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return self
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string $postalCode
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set country
     *
     * @param Application\Document\Country $country
     * @return self
     */
    public function setCountry(\Application\Document\Country $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return Application\Document\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set coordinates
     *
     * @param Application\Document\Coordinates $coordinates
     * @return self
     */
    public function setCoordinates(\Application\Document\Coordinates $coordinates)
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * Get coordinates
     *
     * @return Application\Document\Coordinates $coordinates
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Set distance
     *
     * @param string $distance
     * @return self
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
        return $this;
    }

    /**
     * Get distance
     *
     * @return string $distance
     */
    public function getDistance()
    {
        return $this->distance;
    }
}
