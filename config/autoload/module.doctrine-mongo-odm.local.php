<?php
return array(
    'doctrine' => array(
        'connection' => array(
            'odm_default' => array(
                'server' => 'localhost',
                'port' => '27017',
                'dbname' => 'zend',
                'options' => array()
            ),
        ),
        'configuration' => array(
            'odm_default' => array(
                'default_db' => 'zend'
            )
        )
    )
);